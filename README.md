# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* slack_message


### How do I get set up? ###

* curl -O https://bitbucket.org/newrahmat/slack_message/raw/b4192c89b7bac1ab0d9d287e4b24cecae07ad485/slack_message.sh

### How to use? ###

Basic connection parameters:
 -h hook_url
 -c channel
 -u username
 -i icon
 -p enable|disable (markdown)

Define message using file contents:
 -F "file"
 -C color
 -T "title"

Define message using parameter:
 -m "message"

Otherwise, define message using standard input.

### Post simple message ###

* bash slack_message.sh -h https://hooks.slack.com/services/D21FA21F/F9PATXG7R/38BepMtJ5NxF8yV2fQsHjen1 -c system-operations -u sysopsusr -i penguin -m "$(hostname) timezone is  $(cat /etc/timezone)"

### Post multi-line message ###

* bash slack_message.sh -h https://hooks.slack.com/services/D21FA21F/F9PATXG7R/38BepMtJ5NxF8yV2fQsHjen1 -c system-operations -u sysopsusr -i terminal -C 1974D2 -m "$(hostname) timezone is  $(cat /etc/timezone)\nDatetime is $(date)"

### Post file as a message ###

* bash slack_message.sh -h https://hooks.slack.com/services/D21FA21F/F9PATXG7R/38BepMtJ5NxF8yV2fQsHjen1 -c system-operations -u sysopsusr -i terminal -F /proc/mdstat -T "$(hostname) software raid status"

### Post file as a message ###

* df -h | sed -e '1s/^/```/' -e '$a```' | bash slack_message.sh -h https://hooks.slack.com/services/D21FA21F/F9PATXG7R/38BepMtJ5NxF8yV2fQsHjen1 -c system-operations -u sysopsusr -i comet -C 1974D2 -p enable -T "$(hostname) system disk space usage" 


